ARG HELM_VERSION
ARG KUBERNETES_VERSION

FROM registry.gitlab.com/thorgate-public/tg-autodevops-helm-image/release/helm-${HELM_VERSION}-kube-${KUBERNETES_VERSION}:latest

COPY src/ build/

# Install Dependencies
RUN apk add -U curl tar gzip bash py-pip

RUN pip install awscli

RUN ln -s /build/bin/* /usr/local/bin/
